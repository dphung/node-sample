const MongodbMemoryServer = require('mongodb-memory-server');
const request = require('supertest');

const blogData = require('../data/blog.json');
const app = require('../../server');
const mongo = require('../../shared/mongo');

const model = {
  mongod: null,
  async setupMongo() {
    const MONGO_DB_NAME = 'jest';
    this.mongod = new MongodbMemoryServer.default({
      instance: {
        dbName: MONGO_DB_NAME,
      },
    });
    return mongo.connect(
      await this.mongod.getConnectionString(),
      MONGO_DB_NAME,
    );
  },

  async tearDownMongo() {
    await mongo.close();
    return await this.mongod.stop();
  },

  async resetData() {
    await Promise.all([mongo.db.collection('blog').remove()]);

    return Promise.all([mongo.db.collection('blog').insert(blogData)]);
  },

  async request(url, body) {
    return request(app)
      .post(url)
      .set('authorization', model.clientToken())
      .send(body);
  },

  mapToJsonObject(obj) {
    return JSON.parse(JSON.stringify(obj));
  },
};

module.exports = model;
