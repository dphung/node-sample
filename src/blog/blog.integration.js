const request = require('supertest');

const Blog = require('./Blog');
const app = require('../server');
const data = require('../misc/data/blog.json');
const Helper = require('../misc/test/helper');

describe('Blog Routes:', () => {
  beforeAll(async () => Helper.setupMongo());
  beforeEach(async () => Helper.resetData());
  afterAll(async () => Helper.tearDownMongo());

  it('should find blog by id', async () => {
    const blog = new Blog(data[0]);

    const { body } = await request(app)
      .get(`/blog/${blog._id}`)
      .expect(200);

    expect(body).toEqual(Helper.mapToJsonObject(blog));
  });

  it('should create new blog', async () => {
    const blog = new Blog({
      title: 'New title',
      body: 'hello world',
      date: new Date(),
      url: 'https://www.google.com',
    });

    const { body } = await request(app)
      .post('/blog')
      .send(blog)
      .expect(200);

    expect(body).toEqual(Helper.mapToJsonObject(blog));
  });

  it('should update blog by id', async () => {
    const blog = new Blog(data[0]);
    blog.title = 'new title';

    const { body } = await request(app)
      .put(`/blog/${blog._id}`)
      .send(blog)
      .expect(200);

    expect(body.title).toEqual(blog.title);
  });

  it('should remove blog by id', async () => {
    const blog = new Blog(data[0]);

    const { body } = await request(app)
      .delete(`/blog/${blog._id}`)
      .expect(200);

    expect(body.ok).toEqual(1);
  });
});
