const router = require('express').Router();

const service = require('./blog.service');

router.get('/:id', (req, res, next) => {
  service.findOne(req.params.id).then((result) => res.send(result), next);
});

router.post('/', (req, res, next) => {
  service.create(req.body).then((result) => res.send(result), next);
});

router.put('/:id', (req, res, next) => {
  service.update(req.body).then((result) => res.send(result), next);
});

router.delete('/:id', (req, res, next) => {
  service.remove(req.params.id).then((result) => res.send(result), next);
});

module.exports = router;
