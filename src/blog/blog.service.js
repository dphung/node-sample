const { ObjectId } = require('bson');

const Blog = require('./Blog');
const mongo = require('../shared/mongo');

const service = {
  async findOne(id) {
    const criteria = { _id: new ObjectId(id) };
    return mongo.db.collection('blog').findOne(criteria);
  },

  async create(payload) {
    const blog = new Blog(payload);

    return (await mongo.db
      .collection('blog')
      .findOneAndUpdate({ _id: blog._id }, blog, { upsert: true, returnOriginal: false })).value;
  },

  async update(payload) {
    const blog = new Blog(payload);

    return (await mongo.db
      .collection('blog')
      .findOneAndUpdate({ _id: blog._id }, { $set: blog }, { returnOriginal: false })).value;
  },

  async remove(id) {
    return (await mongo.db.collection('blog').deleteOne({ _id: new ObjectId(id) })).result;
  },
};

module.exports = service;
