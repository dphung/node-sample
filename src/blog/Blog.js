const { ObjectId } = require('bson');

class Blog {
  constructor(opt) {
    this._id = null;
    this.title = null;
    this.body = null;
    this.date = null;
    this.url = null;

    if (opt) {
      this.fillObject(opt);
    }
  }

  fillObject(opt) {
    this._id = opt._id ? new ObjectId(opt._id) : new ObjectId();
    this.title = opt.title;
    this.body = opt.body;
    this.date = opt.date ? new Date(opt.date) : null;
    this.url = opt.url;
  }
}

module.exports = Blog;
