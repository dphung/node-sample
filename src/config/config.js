module.exports = {
  MONGODB_URI: process.env.MONGODB_URI || 'mongodb://localhost:27017/sample',
  MONGODB_DATABASE: process.env.MONGODB_DATABASE || 'sample',
};
