const MongoClient = require('mongodb').MongoClient;

var service = {
  client: null,
  db: null,

  async connect(url, databaseName) {
    try {
      this.client = await MongoClient.connect(
        url,
        { reconnectTries: Number.MAX_VALUE },
      );
      return this.switchDatabase(databaseName);
    } catch (err) {
      throw new Error('Can not connection to mongo.');
    }
  },

  async close() {
    return this.client.close();
  },

  switchDatabase(name) {
    this.db = this.client.db(name);
    return this.db;
  },
};

module.exports = service;
