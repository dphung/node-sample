const app = require('./src/server');
const Helper = require('./src/misc/test/helper');

const service = {
  async run() {
    await Helper.setupMongo();
    await Helper.resetData();

    await app.listen(3000);
    console.info(`Express server listening on port 3000 in ${app.get('env')} mode`);
  },
};

service.run();
